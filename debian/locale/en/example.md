# PRESENTATION TITLE

This is a **test** of the `slides` in *markdown*. Here we have **bold** text, \
*italic* text, <u>sublining</u> or ~~cutting~~ text, plus a combination of \
***bold and italic*** and the simulation of a [link](url).


## HERE WE HAVE A SUBTITLE

We also interpret `inline code` and blocks of code:

```
for n in $teste; do
    echo $n
done
```

Code blocks can be used to demonstrate markings:

```
LINK: [label](url)
```

### NOW IT'S A SUB-TITLE!

Cool, isn't it? How about seeing an unordered list?

- The source text will render correctly on any platform \
  that interprets GitHub markdown-compatible markup. Items in lists can be \
  given **styles**! 

- Like **negrite**, *italic*, and <u>sublime</u>.

- Besides the `codes` and the [simulated links](url)!

What about an ordered list?

1. item one
1. item two
1. item three


## LOOK AT THE QUOTE RIGHT THERE!

> **Important:** There are limitations, after all, this parser is intended to \
display highlights and colors on the terminal. The main advantage, however, \
is that the source text will render correctly on any platform that interprets \
GitHub-compliant markdown.

***That's all, folks!***
